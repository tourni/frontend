import Link from '../components/link'
import styled from 'styled-components'

const Game = styled.span`
    opacity: 0.8;
    padding-right: 1rem;
`;
const Listing = ({tournament}) => <React.Fragment>

    <hr style={{opacity: 0.2}}/>
    <Link href={`tournament?name=${tournament.name}`}>
        <li className='list-item' key={tournament.name}>
            { tournament.game ? <Game>{tournament.game}</Game> : null}
            <span className='has-text-weight-bold'>
                {tournament.name}
            </span>
            <data style={{float: 'right'}}> <span style={{fontSize: '0.7rem'}}>Prize pool:</span> {tournament.prize}</data>
        </li>
    </Link>
</React.Fragment>
export default Listing