import styled from 'styled-components'

const Box = styled.div`
    padding: 2rem;
    @media (min-width: 600px) {
        padding: 4rem;
        border-radius: 5px;
    }
    min-height: 50vh;
	box-shadow: 0 5px 10px rgba(50,50,50,0.4), 0 15px 40px rgba(50,50,50,0.5);
`;

export default ({children}) => <Box className='mybox'>
    {children}
</Box>