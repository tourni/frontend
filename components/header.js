import React from 'react'
import styled from 'styled-components'
import Link from '../components/link';

const Nav = styled.nav`
    z-index:2;
    width: 100%;
    padding: 1rem;
    box-shadow: 0 5px 20px rgba(20,20,30,0.3), 0 15px 40px rgba(50,50,50,0.2);
    display: flex;
    @media (max-width: 500px) {
        flex-direction: column;
        & > * {
            padding: 1rem;
        }
    }
    @media (max-width: 900px) {
        align-items: center;

    }
    @media (min-width: 900px) {
    position: fixed;
        max-width: 15rem;
        flex-direction: column;
        height: 100%;
    }
`;
const Beta = styled.sub`
    font-size: 100%;
    display: inline;
    position: relative;
    margin-left: 5px;
    opacity: 0.7;
`;

const Links = styled.ul`
    flex-grow: 2;
    text-transform: uppercase;
    letter-spacing: 1px;
    @media (min-width: 900px) {
        padding-top: 2rem;
    }
    font-size: 90%;
`;
const NL = styled.li`
    padding: 1rem;
    display: inline;
    @media (min-width: 900px) {
        display: block;
    }
`;
const Navlink = ({title, url}) => (
    <Link href={url} >
        <NL>
            {title}
        </NL>
    </Link>
)

export default () => <Nav>
    <Link style={{display: 'block'}} href='/'>
        <div style={{paddingLeft: '1rem'}}>
            <img style={{display: 'block' }} src='/static/knight.svg'/>
            <h1 style={{display: 'inline'}} className='is-size-3' >
                Tournee
            </h1>
            <Beta>
                BETA
            </Beta>
        </div>
    </Link>
    <Links>
        <Navlink title='Games' url='/'/>
        <Navlink title='Tournaments' url='/tournaments'/>
        <Navlink title='Organise' url='/organise'/>
    </Links>
    <button  className='button' disabled>
        Signup/Login
    </button>
</Nav>