import React from 'react'
import styled from 'styled-components'

const Column = styled.div`
    max-width: 1100px;
    margin: auto;
    padding: 0 4rem;
    box-sizing: border-box;
    @media (max-width: 600px) {
        padding: 0;
    }
`;

export default Column