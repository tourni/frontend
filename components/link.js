import styled from 'styled-components'
import Link from 'next/link'

const L = styled.a`
    color: inherit;

    &:hover {
        cursor: pointer;
    }
`;
const link = ({href, children}) => (
    <Link prefetch href={href}>
        <L>
        {children}
        </L>
    </Link>

)
export default link;