import Document from 'next/document'
import { ServerStyleSheet as StyledSheets } from 'styled-components'

export default class MyDocument extends Document {
  static async getInitialProps (ctx) {
    const styled_sheets = new StyledSheets()

    const originalRenderPage = ctx.renderPage

    try {
      ctx.renderPage = () =>
        originalRenderPage({
          enhanceApp: App => props => styled_sheets.collectStyles(<App {...props} />)
        })

      const initialProps = await Document.getInitialProps(ctx)
      return {
        ...initialProps,
        styles: (
          <>
            {initialProps.styles}
            {styled_sheets.getStyleElement()}
          </>
        )
      }
    } finally {
      styled_sheets.seal()
    }
  }
}