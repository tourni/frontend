import React from 'react'
import games from '../static/games';
import Page, { PageTitle } from '../layouts/page'
import Column from '../components/column'
import Box from '../components/box'
import Listing from '../components/listing'

const tournaments = ({tournaments}) => (
    <Page bg={'/static/gamer.jpeg'}>
    <Column>
        <PageTitle>
            All tournaments:
        </PageTitle>
        <Box>
        <h2 className='subtitle'>Open for participants:</h2>
        <ul>
            {
                tournaments.map(
                    tournament => <Listing tournament={tournament}/>
                )
            }
        </ul>
        </Box>
    </Column>
    </Page>
)

tournaments.getInitialProps = () => {
    const tournaments = games.map(
        game => game.tournaments.map(
            t => ({...t, game: game.title})
        )
    ).flat();

    console.log(tournaments)

    return {
        tournaments
    }
}


export default tournaments
