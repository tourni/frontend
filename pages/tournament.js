import React from 'react'
import Column from '../components/column';
import games from '../static/games'
import Page, { PageTitle } from '../layouts/page';
import styled from 'styled-components';
import Box from '../components/box';
const Section = ({title, children}) => (
    <div style={{margin: '2rem 0'}}>
        <h3 className='subtitle is-3'>
            {title}
        </h3>
        <hr/>
        {children}
    </div>
)

const tournaments = ({tournament, game}) => (
    <Page bg={game.cover_image}>
        <Column>
            <PageTitle>
                {game.title} Tournament
            </PageTitle>
            <Box>
                <Section title='Name'>
                    <span className='is-size-4'>
                        {tournament.name}
                    </span>
                </Section>
                <Section title='Details'>
                    {tournament.details}
                </Section>
                <Section title='Prize pool (current)'>
                    <span className='is-size-3'>{tournament.prize} TNC</span>
                    <button disabled style={{display: 'inline-block', float: 'right'}} className='button'>
                        Add to pot
                    </button>
                </Section>
                <Section title='Match schedule'>
                    <img style={{display: 'block', margin: '3rem auto'}} src={tournament.match_image}/>
                </Section>
                <Section title='Watch'> {
                    tournament.watch_links.map(l => <a href={l}>{l}</a>)
                } </Section>
            </Box>
        </Column>
    </Page>
)

tournaments.getInitialProps = async (context) => {

    let game = games
        .find(e => e.tournaments.find(e => e.name == context.query.name));

    return {
        game,
        tournament: game.tournaments.find(e => e.name == context.query.name),
    }
}
export default tournaments