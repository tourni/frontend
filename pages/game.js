import React from 'react'
import styled from 'styled-components'
import Column from '../components/column'
import Page, { PageTitle } from '../layouts/page'
import games from '../static/games'
import Box from '../components/box'
import Listing from '../components/listing'



const game = ({game}) => {
    return <Page bg={game.cover_image}>
        <Column>
            <PageTitle>
                {game.title} tournaments
            </PageTitle>
            <Box>
                <h2 className='subtitle'>Open for participants:</h2>
                <ul>
                    {
                        game.tournaments.map(
                            tournament => <Listing tournament={tournament}/>
                        )
                    }
                </ul>
            </Box>
        </Column>
    </Page>
}
game.getInitialProps = async (context) => {
    console.log(context.query.title);

    return {
        game: games.find(e => e.title === context.query.title)
    }
}



export default game
