import React from 'react'
import Page, {PageTitle} from '../layouts/page'
import Box from '../components/box';
import Column from '../components/column';

const organise = () => {
    return (
        <Page bg={'/static/organise.jpeg'}>
            <Column>
            <PageTitle>
                Create an event
            </PageTitle>
            <Box className='content'>

                <h5 className='subtitle is-5'>
                    Automated event listing is still in the oven...
                </h5>
                <p>For now, contact us via the google forms link below, and we'll get your event listed!</p>
            
                
            </Box>
            </Column>
        </Page>
    )
}

export default organise
