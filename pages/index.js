import React from 'react'
import styled from 'styled-components'
import Column from '../components/column'
import Link from '../components/link'
import Page, { PageTitle } from '../layouts/page'
import games from '../static/games'
import Box from '../components/box'

const Tagline = styled.div`
    padding: 5rem 3rem;
    @media (max-width: 700px) {
        padding: 5rem 0;
    }
`;
const Grid = styled.div`
    @media (min-width: 1200px) {
        display: grid;
        grid-template-columns: 1fr 1fr 1fr;
		grid-gap: 1rem;
		padding: 1rem;
    }
`;
const Tile = styled.div`
    height: 300px;
	color: whitesmoke;
	box-shadow: 0 5px 10px rgba(154,160,185,.6), 0 15px 40px rgba(166,173,201,.2);

    background: linear-gradient( rgba(0,0,0,0.0), rgba(0,0,0,0.6) ), url(${props => props.background_image});
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;

    display: flex;
    align-items: flex-end;
    justify-content: center;
    :hover {
        filter: brightness(120%);
	}
    &.disabled {
        filter: grayscale(100%);
        opacity: 70%;
    }
    @media (min-width: 900px) {
		border-radius: 5px;
	}

`;
const TextArea = styled.div`
    padding: 2rem 4rem;
    width: 100%;
`;

const Home = ({games}) => {
    return (
        <Page bg={'/static/gamer.jpeg'}>
            <Column>
            <PageTitle>
                Gamers rise up... <sub style={{display: 'block', textAlign: 'right'}}>(to the challenge of an honourable contest for cryptocurrency)</sub>
            </PageTitle>
            <Box>
                <div style={{display: 'flex', flexDirection: 'row', alignItems:'center', justifyContent: 'center'}}>
                    <img style={{width: '20%'}} src='/static/logo.svg'/>
                </div>
            <Tagline>
                <p className='subtitle'>The next-generation, community-powered <b>eSports network</b>, with prize pools crowdfunded by <b>cryptocurrency</b>.</p> 
            </Tagline>
            <Grid>
                {
					games.map(game => game.supported == 1 ? (
                            <Link key={game.title} href={game.url}>
                                <Tile background_image={game.cover_image}>
                                    <TextArea>
                                    <h2 className='subtitle ' style={{color: 'whitesmoke'}}>
                                            {game.title}
                                        </h2>
                                    </TextArea>
                                </Tile>
                            </Link>
						) : (
							<Tile key={game.title} className={"disabled"} background_image={game.cover_image}>
								<TextArea>
									<h2 className='subtitle' style={{color: 'whitesmoke'}}>
										Coming soon...
									</h2>
								</TextArea>
							</Tile>
						)
					)
                }

            </Grid>
            </Box>
            </Column>
        </Page>
    )
}

Home.getInitialProps = async () => {

    let g = games
        .map(game => ({
            ...game,
            url: `/game?title=${game.title}`
        }))

	return {
		games: g
	}	
}
export default Home