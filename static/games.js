export default [
    {
        title: "CS:GO",
        cover_image: "/static/CSGO_wallpaper.jpg",
        supported: true,
        tournaments: [
            {
                name: "Example tournament",
                details: "This is an example of the tournament format, please contact via the 'organise' link up top to list your tournament here!",
                watch_links: [
                    "http://twitch.tv/not-real-link",
                ],
                prize: 999.9,
                start_date: "2019-06-29",
                match_image: "/static/matches.svg"
            }
        ]
    },
    {
        title: "League of Legends",
        cover_image: "/static/league.jpg",
        supported: false,
        tournaments: []
    },
    {
        title: "Dota 2",
        cover_image: "/static/Dota2.jpeg",
        supported: false,
        tournaments: []
    },
    {
        title: "PUBG",
        cover_image: "/static/PUBG.jpg",
        supported: false,
        tournaments: []
    },
    {
        title: "Rainbow Six Siege",
        cover_image: "/static/rainbow6.png",
        supported: false,
        tournaments: []
    },
    {
        title: "Fortnite",
        cover_image: "/static/fortnite.jpeg",
        supported: false,
        tournaments: []
    },
]