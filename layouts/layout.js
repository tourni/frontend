import React from 'react'
import styled from 'styled-components'
import Head from 'next/head'
import Link from '../components/link'
import Navbar from '../components/header'
const Footer = styled.footer`
    padding: 2rem 0;
    margin-top: 4rem;
    font-size: 80%;
    a {
        margin: 0 4px;
        font-weight: bold;
        color: inherit;
    }
`;
const Main = styled.main`
    @media (min-width: 900px) {
        padding-left: 15rem;
    }
`;
export default ({children}) => {
    return <React.Fragment>
        <Head>
            <title>Tournee</title>

            <link rel="apple-touch-icon" sizes="180x180" href="/static/apple-touch-icon.png"/>
            <link rel="icon" type="image/png" sizes="32x32" href="/static/favicon-32x32.png"/>
            <link rel="icon" type="image/png" sizes="16x16" href="/static/favicon-16x16.png"/>
            <link rel="manifest" href="/static/site.webmanifest"/>
            <link rel="mask-icon" href="/static/safari-pinned-tab.svg" color="#5bbad5"/>
            <meta name="msapplication-TileColor" content="#da532c"/>
            <meta name="theme-color" content="#ffffff"/>
        </Head>
        <Navbar/>
        <Main>
            {children}
            <Footer style={{opacity: 0.4, color: 'whitesmoke'}} className="content has-text-centered">
                Icons made by 
                <a href="https://www.freepik.com/" title="Freepik">
                     Freepik 
                </a>
                from 
                <a href="https://www.flaticon.com/" title="Flaticon">
                    www.flaticon.com
                </a>
                     is licensed by 
                <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">
                    CC 3.0 BY
                </a>
            </Footer>
        </Main>
    </React.Fragment>
}