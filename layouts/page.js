import styled from 'styled-components'
const Pic = styled.img`
    filter: saturate(60%) contrast(30%) brightness(70%);
    position: fixed;
    height: 100%;
    min-width: 100%;
    object-fit: cover;
    z-index: -1;
    box-shadow: 0 5px 10px rgba(50,50,50,0.4), 0 15px 40px rgba(50,50,50,0.5);
    @media (max-width: 900px) {
        top: 0;
    }
`;

export default ({bg, children}) => (
    <React.Fragment>
        <Pic src={bg}/>
        {children}
    </React.Fragment>
)

export const PageTitle = styled.h2`
    letter-spacing: 1px;
    font-size: 3rem;
    color: whitesmoke;
    padding: 6rem 0;
    text-shadow: 2px 2px 1px rgb(30,30,30, 0.9);
    @media (max-width: 700px) {
        font-size: 2rem;
        padding: 6rem 2rem;
    }
`;