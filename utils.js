export const save_item = (key, value) => { window.localStorage.setItem(key, JSON.stringify(value)) };
export const get_item = (key) => JSON.parse(window.localStorage.getItem(key));
